# NodePostgres

Prueba de conexión de NodeJS a base de datos PostgreSQL

- Instalar dependencias 

> npm i pg express ejs nodemon

- Script de la base de datos está en la carpeta **sql**

- Comando para iniciar el servidor

> npm start

- Archivos de la aplicación

    - index.js Inicia el servidor

    - db.js Contiene la configuración y la conexión a la base de datos PostgreSQL

    - routes/products.js Contiene todas las rutas y acciones del API

- Rutas GET para renderizar las vistas

    - /create-products Vista para crear producto

    - /create-detail Vista para crear detalle de producto

    - /list-products Vista para listar los productos ingresados

    - /search-detail Vista para buscar detalles de producto por código o ID. Aquí se selecciona el producto y se muestran en el formulario los datos que corresponden al detalle.

    - /search-byid/:id Ruta interna para buscar recogiendo el parámetro id de la lista desplegable que se encuentra en la vista de búsqueda de detalles de producto.

- Rutas POST para guardar datos

    - /save-product Recoge los valores del formulario de la vista para crear producto y los guarda en la tabla products de la base de datos PostgreSQL

    - /save-detail Recoge los valores del formulario de la vista para crear detalles de producto y los guarda en la tabla detalleproductos de la base de datos PostgreSQL

**DATOS DEL FRONTEND**

La carpeta interna donde están los archivos de las vistas es public, ahí están los archivos CSS, JS y EJS (Vistas).

Este frontend está conformado por

- Html para elementos como formularios, campos de texto, campos de listas desplegables, botones.
- CSS apoyado en Boostrap para ayudar en la maquetación y tener algunos estilos que mejoren en looknfeel
- Javascript apoyado con jQuery para manejar los eventos de los controles y también para hacer el consumo de la API construida en el backend pasando y recibiendo información en formato JSON por POST o enviando datos por GET en caso particular de la búsqueda por detalle de productos. Todas las acciones descritas anteriormente están en el archivo public/js/main-app.js
