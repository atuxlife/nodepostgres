const express = require('express');
const path = require('path');
const app = express();

// Settings
app.set('port', process.env.PORT || 3000 );
app.set('json spaces',2);

// Middlewares
app.use(express.urlencoded({extended: false}));
app.use(express.json());

// Plantillas estáticas
app.use(express.static('public'));
app.set('views', __dirname + "/public/html");
app.set('view engine', 'ejs');

// Routes
app.use(require('./routes/products'));

// Starting the server
app.listen(app.get('port'), () => {
    console.log('Server on port '+app.get('port'));
});