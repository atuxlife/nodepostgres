const {Router} = require('express');
const router = Router();
const db = require('../db');

router.get('/', (req, res) => {
    res.render("create-products");
});

router.get('/create-products', (req, res) => {
    res.render("create-products");
});

router.post('/save-product', async (req, res) => {

    const { nombre, descri, precio } = req.body;

    const insrt = await db.query(
        'INSERT INTO productos(nombre, descripcion, precio) VALUES ($1, $2, $3) RETURNING *', 
        [nombre, descri, precio]);

    let httpCod = 0, jsonMsg = {};

    if( insrt.rowCount == 1 ){
        httpCod = 200;
        jsonMsg = {
            respons: 'Ok',
            message: 'Producto agregado exitosamente!',
            body: {
                producto: { nombre, descri, precio }
            }
        }
    } else {
        httpCod = 500;
        jsonMsg = {
            respons: 'Error',
            message: "Hubo un problema insertando el producto "+error,
            body: {
                producto: { nombre, descri, precio }
            }
        };
    }

    res.status(httpCod).send(jsonMsg);
    
});

router.get('/create-detail', async (req, res) => {

    let sql = `SELECT p.idproducto, p.nombre, p.precio 
               FROM productos p
               ORDER BY 1 DESC;`;
    
    const products = await db.query(sql);

    res.render("create-detail", {'products':products.rows});

});

router.post('/save-detail', async (req, res) => {

    const { producto, cantidad, valortot, valoriva } = req.body;

    const insrt = await db.query(
        'INSERT INTO detalleproductos(idproducto, cantidad, valortotal, valoriva) VALUES ($1, $2, $3, $4) RETURNING *', 
        [producto, cantidad, valortot, valoriva]);

    let httpCod = 0, jsonMsg = {};

    if( insrt.rowCount == 1 ){
        httpCod = 200;
        jsonMsg = {
            respons: 'Ok',
            message: 'Detalle de producto agregado exitosamente!',
            body: {
                detprod: { producto, cantidad, valortot, valoriva }
            }
        }
    } else {
        httpCod = 500;
        jsonMsg = {
            respons: 'Error',
            message: "Hubo un problema insertando el detalle de producto "+error,
            body: {
                detprod: { producto, cantidad, valortot, valoriva }
            }
        };
    }

    res.status(httpCod).send(jsonMsg);

});

router.get('/list-products', async (req, res) => {
    
    let sql = `SELECT p.idproducto, p.nombre, p.descripcion, p.precio 
               FROM productos p
               ORDER BY 1 DESC;`;
    
    const products = await db.query(sql);

    res.render("list-products", {'products':products.rows});

});

router.get('/search-detail', async (req, res) => {

    let sql = `SELECT p.idproducto, p.nombre 
               FROM productos p
               ORDER BY 1 DESC;`;
    
    const products = await db.query(sql);

    res.render("search-detail", {'products':products.rows});

});

router.get('/search-byid/:id', async (req, res) => {

    const id = parseInt(req.params.id);
    let sql = `SELECT d.cantidad, d.valortotal, d.valoriva
                FROM detalleproductos d
                WHERE d.idproducto = $1
                LIMIT 1;`;
    const detalle = await db.query(sql, [id]);

    if( detalle.rowCount == 1 ){
        res.status(200).send({ 'stat' : 'OK', 'message' : 'Hay resultados para este detalle', 'datos' : detalle.rows });
    } else {
        res.status(200).send({ 'stat' : 'ERROR', 'message' : 'No hay resultados para este detalle', 'datos' : '' });
    }

    

});

module.exports = router;