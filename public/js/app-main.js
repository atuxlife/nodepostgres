var server = 'http://localhost:3000';

// Serializar formulario
$.fn.formToObject = function(){
    let o = {};
    let a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

// Validación de campos vacíos de un formulario específico
function valReqFrm(frm){
    let state = true;
    let formu = '#'+frm;
    let campos = $(formu+' input[type="text"]:required, '+formu+' input[type="date"]:required, '+formu+' [type="number"]:required, '+formu+' input[type="file"]:required, '+formu+' textarea:required, '+formu+' select:required');
    $(campos).each(function() {
        if($(this).val()==''){
            state = false;
            $(this).addClass('is-invalid');
            var lbl = $(this).parent().find('label').text();
            $(this).parent().append('<div class="invalid-feedback">Debe diligenciar el campo '+lbl.slice(0, -2)+' </div>');
        }
    });
    return state;
}

$(document).on('click','input, select, textarea',function() {
    if( $(this).hasClass('is-invalid') ){
        $(this).removeClass('is-invalid');        
    }
});

// Resetear formulario
$.fn.clearForm = function(tag1) {
    tag1 = tag1 || 'form';
    return this.each(function() {
        let type = this.type, tag = this.tagName.toLowerCase();
        if (tag == tag1)
            return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' || tag == 'textarea' || type == 'date' || type == 'hidden')
            this.value = '';
        else if (type == 'email' || type == 'number' || type == 'file')
            this.value = '';
        else if (type == 'checkbox' || type == 'radio')
            this.checked = false;
        else if (tag == 'select'){
            if (this.getAttribute("multiple") == null) {
                this.value = '';
                this.selectedIndex = 0;
            }else{
                this.value = '';
                this.selectedIndex = -1;
            }
        }
    });
}

// Acciones del botón nuevo producto
$(document).on('click','#btnSaveProduct',function(){

    if( valReqFrm('products') ){

        let frmData = {
            'nombre' : $("#txtNombre").val(),
            'descri' : $("#txtDescripcion").val(),
            'precio' : $("#txtPrecio").val()
        }
        
        $.ajax({
            url: server+'/save-product',
            type: "post",
            contentType : "application/json",
            dataType: "json",
            data: JSON.stringify(frmData),
            cache: false,
            success: function($dres) {
                if( $dres.respons == 'Ok' ){
                    toastr.success($dres.message);
                    $("#products").clearForm();
                    $("#txtNombre").focus();
                } else {
                    toastr.error($dres.message);
                }
            }
        });

    } else {
        toastr.error('Debe llenar todos los campos marcados como obligatorios (asterisco * rojo) para poder guardar.');
    }

});

// Acciones del botón nuevo producto
$(document).on('click','#btnSaveDetail',function(){

    if( valReqFrm('details') ){

        let prod = $("#slcProducto").val();
        let strSplit = prod.split('|'); 

        let frmData = {
            'producto' : strSplit[0],
            'cantidad' : $("#txtCantidad").val(),
            'valortot' : parseInt($("#txtCantidad").val()) * parseInt(strSplit[1]),
            'valoriva' : (parseInt($("#txtCantidad").val()) * parseInt(strSplit[1])) * 0.19
        }

        $.ajax({
            url: server+'/save-detail',
            type: "post",
            contentType : "application/json",
            dataType: "json",
            data: JSON.stringify(frmData),
            cache: false,
            success: function($dres) {
                if( $dres.respons == 'Ok' ){
                    toastr.success($dres.message);
                    $("#details").clearForm();
                    $("#slcProducto").focus();
                } else {
                    toastr.error($dres.message);
                }
            }
        });

    } else {
        toastr.error('Debe llenar todos los campos marcados como obligatorios (asterisco * rojo) para poder guardar.');
    }

});

// Acciones del botón nuevo producto
$(document).on('change','#slcProductoSearch',function(){

    let prd = $(this).val();

    if( prd.length == 0 ){
        $("#txtCantidad").val('');
        $("#txtValTotal").val('');
        $("#txtValIVA").val('');
    } else {

        $.ajax({
            url: server+'/search-byid/'+prd,
            type: "get",
            contentType : "application/json",
            dataType: "json",
            cache: false,
            success: function($dres) {

                if( $dres.stat == 'OK' ){
                    toastr.success($dres.message);
                    $("#txtCantidad").val($dres.datos[0].cantidad);
                    $("#txtValTotal").val($dres.datos[0].valortotal);
                    $("#txtValIVA").val($dres.datos[0].valoriva);
                } else {
                    toastr.error($dres.message);
                    $("#txtCantidad").val('');
                    $("#txtValTotal").val('');
                    $("#txtValIVA").val('');
                }
                
            }
        });

    }

});

// Crear datatable
function renderDT(){
    $('.newrone-table').dataTable({
        'aLengthMenu'       : [[5,10, 25, 50, -1], [5,10, 25, 50, "Todos"]],
        //'aLengthMenu'       : [[5], [5]],
        'iDisplayStart'     :   0,
        'iDisplayLength'    :   10,
        'aaSorting'         :   [[ 0, 'desc' ]],
        "oLanguage" : {
            "sProcessing"       :   "Procesando...",
            "sLengthMenu"       :   "Mostrar _MENU_ artículos",
            "sZeroRecords"      :   "No se encontraron resultados",
            "sEmptyTable"       :   "Ningún dato disponible en esta tabla",
            "sInfo"             :   "Artículos del _START_ al _END_ de un total de _TOTAL_ artículos",
            "sInfoEmpty"        :   "Artículos del 0 al 0 de un total de 0 artículos",
            "sInfoFiltered"     :   "(filtrado de un total de _MAX_ artículos)",
            "sInfoPostFix"      :   "",
            "sSearch"           :   "Buscar:",
            "sUrl"              :   "",
            "sInfoThousands"    :   ",",
            "sLoadingRecords"   :   "Cargando...",
            "oPaginate": {
                "sFirst"    :   "Primero",
                "sLast"     :   "Último",
                "sNext"     :   "Sig.",
                "sPrevious" :   "Ant."
            },
            "oAria": {
                "sSortAscending"    :   ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending"   :   ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}