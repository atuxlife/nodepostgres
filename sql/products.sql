--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

-- Started on 2021-05-10 02:54:57

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 18966)
-- Name: detalleproductos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detalleproductos (
    iddetalleproducto integer NOT NULL,
    idproducto integer,
    cantidad integer,
    valortotal integer,
    valoriva double precision
);


ALTER TABLE public.detalleproductos OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 18964)
-- Name: detalleproductos_iddetalleproducto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detalleproductos_iddetalleproducto_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detalleproductos_iddetalleproducto_seq OWNER TO postgres;

--
-- TOC entry 3002 (class 0 OID 0)
-- Dependencies: 202
-- Name: detalleproductos_iddetalleproducto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detalleproductos_iddetalleproducto_seq OWNED BY public.detalleproductos.iddetalleproducto;


--
-- TOC entry 201 (class 1259 OID 18958)
-- Name: productos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.productos (
    idproducto integer NOT NULL,
    nombre character varying(100),
    descripcion character varying(100),
    precio integer
);


ALTER TABLE public.productos OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 18956)
-- Name: productos_idproducto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.productos_idproducto_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productos_idproducto_seq OWNER TO postgres;

--
-- TOC entry 3003 (class 0 OID 0)
-- Dependencies: 200
-- Name: productos_idproducto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.productos_idproducto_seq OWNED BY public.productos.idproducto;


--
-- TOC entry 2857 (class 2604 OID 18969)
-- Name: detalleproductos iddetalleproducto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalleproductos ALTER COLUMN iddetalleproducto SET DEFAULT nextval('public.detalleproductos_iddetalleproducto_seq'::regclass);


--
-- TOC entry 2856 (class 2604 OID 18961)
-- Name: productos idproducto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos ALTER COLUMN idproducto SET DEFAULT nextval('public.productos_idproducto_seq'::regclass);


--
-- TOC entry 2996 (class 0 OID 18966)
-- Dependencies: 203
-- Data for Name: detalleproductos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detalleproductos (iddetalleproducto, idproducto, cantidad, valortotal, valoriva) FROM stdin;
1	12	5	1500	285
2	11	3	10500	1995
3	10	5	2250	427.5
4	9	15	7500	1425
\.


--
-- TOC entry 2994 (class 0 OID 18958)
-- Dependencies: 201
-- Data for Name: productos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.productos (idproducto, nombre, descripcion, precio) FROM stdin;
2	Gorra Nike	Gorra Nike	500
3	Camiseta Adidas	Camiseta Adidas	750
4	Camisa Nike	Camisa Nike	1400
5	Correa Gucci	Correa Gucci	750
6	Zapatos Adidas	Zapatos Adidas	1500
7	Algo	Algo	1300
8	Alguna Cosa	Alguna Cosa	1500
9	Pescado	Pescado	500
10	Carne	Carne	450
11	Docena de huevos	Docena de huevos	3500
12	Kilo de harina de trigo	Kilo de harina de trigo	300
\.


--
-- TOC entry 3004 (class 0 OID 0)
-- Dependencies: 202
-- Name: detalleproductos_iddetalleproducto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detalleproductos_iddetalleproducto_seq', 4, true);


--
-- TOC entry 3005 (class 0 OID 0)
-- Dependencies: 200
-- Name: productos_idproducto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.productos_idproducto_seq', 12, true);


--
-- TOC entry 2861 (class 2606 OID 18971)
-- Name: detalleproductos detalleproductos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalleproductos
    ADD CONSTRAINT detalleproductos_pkey PRIMARY KEY (iddetalleproducto);


--
-- TOC entry 2859 (class 2606 OID 18963)
-- Name: productos productos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT productos_pkey PRIMARY KEY (idproducto);


--
-- TOC entry 2862 (class 2606 OID 18972)
-- Name: detalleproductos fk_productos_detalles; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalleproductos
    ADD CONSTRAINT fk_productos_detalles FOREIGN KEY (idproducto) REFERENCES public.productos(idproducto) NOT VALID;


-- Completed on 2021-05-10 02:54:57

--
-- PostgreSQL database dump complete
--

